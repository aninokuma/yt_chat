# YouTube Video QA Bot Documentation

This documentation offers a comprehensive explanation of the Python code that powers a YouTube Video Question-Answering Bot. The code is designed to transcribe YouTube videos, process their content, and provide answers to questions based on the video content. Below, we'll delve into the code's structure, its primary components, and how it operates.


```mermaid
classDiagram
    class Assistant {
        - llm_function: async
        + answer(prompt: str, overthink: bool): str
        + self_reflect_prompt(prompt: str): str
    }

    class ASRSystem {
        - device: str
        - pipe
        + transcribe_audio(audio_path: str): str
    }

    class AbstractSummarizer {
        - model_name: str
        - torch_device: str
        - tokenizer
        - model
        + summarize(input_text: str, chunk_size: int): List[str]
        - _summarize_chunk(chunk: str): str
        - _split_input_into_chunks(input_text: str, chunk_size: int): List[str]
    }

    class SentenceEmbeddingSystem {
        - cross_encoder
        - model
        - lexical_system
        + get_nearest_sentences(sentences, query, precomputed_embeddings, precomputed_lexical): List[str]
        + extractive_summarization(transcript: str): List[str]
        + abstractive_summarization(transcript: str): List[str]
    }

    class TranscriptManagement {
        - redis
        + get_transcript(video_id: str): str
        + set_transcript(video_id: str, transcript: str)
        + delete_transcript(video_id: str)
    }

    class YouTubeVideo {
        - url: str
        - video_id: str
        - audio_dir: str
        - audio_path: str
        - title: str
        - author: str
        - description: str
        - video_metadata: str
        - transcript: str
        - transcript_summary: str
        - sentences: List[str]
        - embeddings
        - bm25
        + _update_video_metadata(yt)
        + download_audio()
        + _process_url()
    }

    class YouTubeVideoProcessor {
        - asr_system: ASRSystem
        - semantic_system: SentenceEmbeddingSystem
        - assistant: Assistant
	- transcript\_manager: TranscriptManagement
        + _populate_transcript(youtube_video: YouTubeVideo)
        + _summarize_transcript(youtube_video: YouTubeVideo)
        + process_youtube_video(youtube_video: YouTubeVideo)
        + answer(youtube_video: YouTubeVideo, query: str): str
    }

    AbstractSummarizer --|> SentenceEmbeddingSystem
    YouTubeVideoProcessor o-- ASRSystem
    YouTubeVideoProcessor o-- SentenceEmbeddingSystem
    YouTubeVideoProcessor o-- Assistant
    YouTubeVideoProcessor o-- TranscriptManagement

```

The class diagram represents a system for processing YouTube videos, extracting information, summarizing content, and answering user queries. It consists of several classes, each with specific responsibilities and interactions.

### `Assistant` Class

The `Assistant` class represents an assistant that interacts with a large language model (LLM) and provides responses to user queries. It has the following methods:

- `llm_function`: An asynchronous function that interacts with the LLM to generate responses.
- `answer(prompt: str, overthink: bool): str`: Asks the LLM a question and returns the response.
- `self_reflect_prompt(prompt: str): str`: Generates a self-reflective prompt based on a given prompt.

### `ASRSystem` Class

The `ASRSystem` class represents an Automatic Speech Recognition (ASR) system for transcribing audio. It has the following attributes and methods:

- `device`: A string representing the device used for ASR.
- `pipe`: A component for ASR processing.
- `transcribe_audio(audio_path: str): str`: Transcribes an audio file to text.

### `AbstractSummarizer` Class

The `AbstractSummarizer` class is responsible for summarizing text using a pre-trained summarizer model. It has the following attributes and methods:

- `model_name`: A string representing the name of the pre-trained summarizer model.
- `torch_device`: A string representing the device used for summarization (CPU or GPU).
- `tokenizer`: A tokenizer for preprocessing input text.
- `model`: The summarizer model itself.
- `summarize(input_text: str, chunk_size: int): List[str]`: Summarizes the input text.
- `_summarize_chunk(chunk: str): str`: Summarizes a single chunk of text.
- `_split_input_into_chunks(input_text: str, chunk_size: int): List[str]`: Splits input text into chunks.

### `SentenceEmbeddingSystem` Class

The `SentenceEmbeddingSystem` class is responsible for sentence embedding and semantic search. It interacts with a cross-encoder model, a sentence transformer model, and a BM25 index. It has the following attributes and methods:

- `cross_encoder`: A cross-encoder model for semantic similarity.
- `model`: A sentence transformer model for sentence embedding.
- `lexical_system`: A BM25 index for document retrieval.
- `get_nearest_sentences(sentences, query, precomputed_embeddings, precomputed_lexical): List[str]`: Retrieves the nearest sentences to a given query.
- `extractive_summarization(transcript: str): List[str]`: Performs extractive summarization on a transcript.
- `abstractive_summarization(transcript: str): List[str]`: Performs abstractive summarization on a transcript.

### `TranscriptManagement` Class

The `TranscriptManagement` class manages transcripts in a Redis database. It has the following attributes and methods:

- `redis`: A connection to the Redis database.
- `get_transcript(video_id: str): str`: Gets the transcript for a given video ID.
- `set_transcript(video_id: str, transcript: str)`: Sets the transcript for a video ID.
- `delete_transcript(video_id: str)`: Deletes the transcript for a video ID.

### `YouTubeVideo` Class

The `YouTubeVideo` class represents a YouTube video and its associated attributes. It has the following attributes and methods:

- `url`: The URL of the video.
- `video_id`: The ID of the video.
- `audio_dir`: The directory where the audio file is stored.
- `audio_path`: The path to the audio file.
- Other attributes such as `title`, `author`, `description`, `video_metadata`, `transcript`, `transcript_summary`, `sentences`, `embeddings`, and `bm25`.
- `_update_video_metadata(yt)`: Updates video metadata based on a YouTube video object.
- `download_audio()`: Downloads the audio of the YouTube video.
- `_process_url()`: Processes the URL to create the audio directory and update video metadata.

### `YouTubeVideoProcessor` Class

The `YouTubeVideoProcessor` class processes YouTube videos by utilizing ASR, semantic search, summarization, and the assistant to answer user queries. It has the following attributes and methods:

- `asr_system`: An ASR system.
- `semantic_system`: A semantic system.
- `assistant`: An assistant.
- `_populate_transcript(youtube_video: YouTubeVideo)`: Populates the transcript attribute of a YouTubeVideo object.
- `_summarize_transcript(youtube_video: YouTubeVideo)`: Summarizes the transcript and populates relevant attributes of a YouTubeVideo object.
- `process_youtube_video(youtube_video: YouTubeVideo)`: Processes a YouTube video by populating its metadata, transcript, and summary.
- `answer(youtube_video: YouTubeVideo, query: str): str`: Generates an answer to a user's query based on the YouTube video and query.

### Relationships

- `Assistant --|> ASRSystem`: The Assistant class uses ASRSystem.
- `AbstractSummarizer --|> SentenceEmbeddingSystem`: AbstractSummarizer utilizes SentenceEmbeddingSystem.
- `YouTubeVideoProcessor o-- ASRSystem`: YouTubeVideoProcessor uses ASRSystem.
- `YouTubeVideoProcessor o-- SentenceEmbeddingSystem`: YouTubeVideoProcessor utilizes SentenceEmbeddingSystem.
- `YouTubeVideoProcessor o-- Assistant`: YouTubeVideoProcessor uses Assistant.


