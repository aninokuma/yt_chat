import asyncio
import nest_asyncio
import numpy as np
import os
import redis
import sys
import httpx
import heapq
import torch
import json
import warnings
from typing import List
from rich.console import Console
from rich.markdown import Markdown
from urllib.parse import urlparse
from transformers import PegasusForConditionalGeneration, PegasusTokenizer
from dotenv import load_dotenv
from pytube import YouTube
from youtube_transcript_api import YouTubeTranscriptApi
from transformers import pipeline, WhisperForConditionalGeneration, WhisperProcessor
from sentence_transformers import SentenceTransformer, CrossEncoder
from rank_bm25 import BM25Okapi
from sentence_transformers import util
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lex_rank import LexRankSummarizer

# Suppress DeprecationWarning for the captions.all() method
warnings.filterwarnings("ignore", category=DeprecationWarning)

# Load environment variables
load_dotenv()

# Define global constants
VERCEL_APP_URL = os.environ["VERCEL_APP_URL"]
AUTHORITY = urlparse(VERCEL_APP_URL).netloc
VERCEL_APP_ENDPOINT = os.environ["VERCEL_APP_ENDPOINT"]
REDIS_CONN_STR = os.environ["REDIS_CONN_STR"]

# Apply Google Colab patches
if sys.platform == "linux":
    nest_asyncio.apply()


async def llm_function(prompt, max_repeated_chunks=3, stream=False):
    """
    Asks the LLM a question and returns the response in chunks.

    Args:
        prompt: The question to ask the LLM.
        max_repeated_chunks: The maximum number of repeated chunks to return before stopping.
        stream: Whether or not to stream the response.

    Returns:
        A list of strings containing the LLM's response in chunks.
    """

    """
    Steps:
    1. Create a new HTTP client.
    2. Construct a POST request to the LLM endpoint.
    3. Set the request headers.
    4. Set the request body to the prompt.
    5. Send the request and wait for the response.
    6. If streaming is enabled, iterate over the response and yield each chunk.
    7. Otherwise, read the entire response into a string and return it.
    8. Close the HTTP client.
    """

    async with httpx.AsyncClient() as client:
        url = f"{VERCEL_APP_URL}{VERCEL_APP_ENDPOINT}"
        headers = {
            "authority": AUTHORITY,
            "accept": "*/*",
            "accept-language": "en-US,en;q=0.9,hi;q=0.8",
            "content-type": "text/plain;charset=UTF-8",
            "dnt": "1",
            "origin": VERCEL_APP_URL,
            "referer": VERCEL_APP_URL,
            "sec-ch-ua": '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "sec-gpc": "1",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36",
        }
        data = {"messages": [{"role": "user", "content": prompt}]}
        response = await client.post(url, json=data, headers=headers)

        prev_chunk = None
        repeated_count = 0

        if stream:
            async for chunk in response.aiter_text():
                if prev_chunk == chunk:
                    repeated_count += 1
                    if repeated_count >= max_repeated_chunks:
                        break
                else:
                    repeated_count = 0
                    prev_chunk = chunk

                yield chunk
        else:
            response_text = ""
            async for chunk in response.aiter_text():
                if prev_chunk == chunk:
                    repeated_count += 1
                    if repeated_count >= max_repeated_chunks:
                        break
                else:
                    repeated_count = 0
                    prev_chunk = chunk

                response_text += chunk

            yield response_text


class Assistant:
    """
    A class for interacting with a large language model (LLM).

    Attributes:
        llm_function: A function that takes a prompt and returns the LLM's response.
    """

    def __init__(self, llm_function):
        """
        Initializes an Assistant object.

        Args:
            llm_function: A function that takes a prompt and returns the LLM's response.
        """
        self.llm_function = llm_function

    async def answer(self, prompt, overthink=False):
        """
        Asks the LLM a question and returns the response.

        Args:
            prompt: The question to ask the LLM.
            overthink: Whether or not the LLM should self-reflect on its previous responses before answering.

        Returns:
            A string containing the LLM's response.
        """

        """
        Steps:
        1. If overthink is True, generate a self-reflective prompt based on the given prompt.
        2. Call the llm_function with the prompt.
        3. Return the LLM's response.
        """

        if overthink:
            prompt = self.self_reflect_prompt(prompt)
        response = ""
        async for chunk in self.llm_function(prompt, stream=True):
            response += chunk
        return response

    def self_reflect_prompt(self, prompt):
        """
        Generates a self-reflective prompt based on the given prompt.

        Args:
            prompt: The prompt to self-reflect on.

        Returns:
            A string containing the self-reflective prompt.
        """

        """
        Steps:
        1. Generate three responses to the given prompt using the llm_function.
        2. Construct a self-reflective prompt based on the three generated responses.
        3. Return the self-reflective prompt.
        """

        generated_responses = [
            self.answer(prompt, stdout=False, stream=False, overthink=False)
            for _ in range(3)
        ]

        self_reflective_prompt = (
            "Spot mistakes in these previous responses and write the improved response learning from all weaknesses of previous answers to the original prompt."
            f"Answer 1: {generated_responses[0]}\n"
            f"Answer 2: {generated_responses[1]}\n"
            f"Answer 3: {generated_responses[2]}\n"
            f"{prompt}\n"
        )
        return self_reflective_prompt


class ASRSystem:
    """
    A class for automatic speech recognition (ASR).

    Attributes:
        device: The device to use for ASR.
        pipe: A Hugging Face pipeline for ASR.
    """

    def __init__(self, device="cuda:0"):
        """
        Initializes an ASRSystem object.

        Args:
            device: The device to use for ASR.
        """
        self.device = device
        self.pipe = pipeline(
            "automatic-speech-recognition",
            model="openai/whisper-medium",
            chunk_length_s=10,
            device=self.device,
        )

    def transcribe_audio(self, audio_path):
        """
        Transcribes an audio file to text.

        Args:
            audio_path: The path to the audio file.

        Returns:
            A string containing the transcribed text.
        """

        """
        Steps:
        1. Open the audio file.
        2. Read the audio data.
        3. Transcribe the audio data using the ASR pipeline.
        4. Return the transcribed text.
        """

        with open(audio_path, "rb") as audio_file:
            audio = audio_file.read()
            prediction = self.pipe(audio, batch_size=8)["text"]
        return prediction


class AbstractSummarizer:
    """
    An abstract class for summarizers.

    This class provides a common interface for summarizers, regardless of the underlying implementation.

    Attributes:
        model_name (str): The name of the pre-trained summarizer model to use.
        torch_device (str): The device to use for summarization. Can be either 'cpu' or 'cuda'.
        tokenizer (PegasusTokenizer): The tokenizer to use for preprocessing the input text.
        model (PegasusForConditionalGeneration): The summarizer model.

    Methods:
        summarize(input_text: str, chunk_size: int = 1024) -> List[str]: Summarizes the given input text.
        _summarize_chunk(chunk: str) -> str: Summarizes a single chunk of text.
        _split_input_into_chunks(input_text: str, chunk_size: int) -> List[str]: Splits the given input text into chunks of the specified size.
    """

    def __init__(self):
        """
        Initializes a new `AbstractSummarizer` object.
        """

        self.model_name = "tuner007/pegasus_summarizer"
        self.torch_device = "cuda" if torch.cuda.is_available() else "cpu"
        self.tokenizer = PegasusTokenizer.from_pretrained(self.model_name)
        self.model = PegasusForConditionalGeneration.from_pretrained(
            self.model_name
        ).to(self.torch_device)

    def summarize(self, input_text: str, chunk_size: int = 1024) -> List[str]:
        """
        Summarizes the given input text.

        Args:
            input_text (str): The input text to summarize.
            chunk_size (int): The maximum size of each chunk of text.

        Returns:
            List[str]: A list of summaries for each chunk of text.
        """

        chunks = self._split_input_into_chunks(input_text, chunk_size)
        summaries = []
        for chunk in chunks:
            summary = self._summarize_chunk(chunk)
            summaries.append(summary)
        return summaries

    def _summarize_chunk(self, chunk: str) -> str:
        """
        Summarizes a single chunk of text.

        Args:
            chunk (str): The chunk of text to summarize.

        Returns:
            str: A summary of the given chunk of text.
        """

        batch = self.tokenizer(
            [chunk],
            truncation=True,
            padding="longest",
            max_length=1024,
            return_tensors="pt",
        ).to(self.torch_device)
        gen_out = self.model.generate(
            **batch,
            max_length=128,
            num_beams=5,
            num_return_sequences=1,
            temperature=1.5,
        )
        output_text = self.tokenizer.batch_decode(gen_out, skip_special_tokens=True)
        return output_text[0]

    def _split_input_into_chunks(self, input_text: str, chunk_size: int) -> List[str]:
        """
        Splits the given input text into chunks of the specified size.

        Args:
            input_text (str): The input text to split.
            chunk_size (int): The maximum size of each chunk of text.

        Returns:
            List[str]: A list of chunks of text.
        """

        chunks = []
        for i in range(0, len(input_text), chunk_size):
            chunk = input_text[i : i + chunk_size]
            chunks.append(chunk)
        return chunks


class SentenceEmbeddingSystem:
    """
    A class for sentence embedding and semantic search.

    Attributes:
        cross_encoder: A cross-encoder model for semantic similarity.
        model: A sentence transformer model for sentence embedding.
        sentences: A list of sentences to be embedded.
        input_embeddings: A list of sentence embeddings.
        bm25: A BM25 index for document retrieval.
        abstractive_summarizer: A AbstractSummarizer object
    """

    def __init__(self, cross_encoder, sentence_transformer, abstractive_summarizer):
        """
        Initializes a SentenceEmbeddingSystem object.
        """
        self.cross_encoder = cross_encoder
        self.model = sentence_transformer
        self.abstract_summarizer = abstractive_summarizer
        self.lexical_system = BM25Okapi

    def get_nearest_sentences(
        self, sentences, query, precomputed_embeddings=None, precomputed_lexical=None
    ):
        """
        Retrieves the nearest sentences to the given query.

        Args:
            query: A string containing the query.

        Returns:
            A list of strings containing the nearest sentences.
        """

        def get_cross_scores(cross_encoder, cross_inp):
            """
            Calculates the cross-encoder scores for the given cross-input.

            Args:
                cross_encoder: A cross-encoder model.
                cross_inp: A list of cross-input pairs.

            Returns:
                A list of cross-encoder scores.
            """
            cross_scores = cross_encoder.predict(cross_inp)
            return cross_scores

        query_embedding = self.model.encode([query])
        input_embeddings = precomputed_embeddings
        if input_embeddings is None:
            input_embeddings = self.model.encode(sentences)
        hits = util.semantic_search(query_embedding, input_embeddings, top_k=10)
        hits = hits[0]
        corpus_indices = [hit["corpus_id"] for hit in hits]
        cross_inp = [(query, sentences[idx]) for idx in corpus_indices]
        cross_scores = get_cross_scores(self.cross_encoder, cross_inp)
        cross_scores = np.array(cross_scores)
        cross_scores = (cross_scores - np.min(cross_scores)) / (
            np.max(cross_scores) - np.min(cross_scores)
        )
        lexical_model = precomputed_lexical
        if lexical_model is None:
            lexical_model = self.lexical_system(sentences)
        bm25_scores = lexical_model.get_scores(query)
        bm25_scores = np.array(bm25_scores)
        bm25_scores = (bm25_scores - np.min(bm25_scores)) / (
            np.max(bm25_scores) - np.min(bm25_scores)
        )
        overall_scores = []
        for idx, scores in enumerate(bm25_scores):
            if idx in corpus_indices:
                overall_scores.append(
                    bm25_scores[idx] * 0.3
                    + 0.7 * cross_scores[corpus_indices.index(idx)]
                )
            else:
                overall_scores.append(bm25_scores[idx])
        top_5_scores = heapq.nlargest(8, overall_scores)
        nearest_sentences = [sentences[overall_scores.index(x)] for x in top_5_scores]
        return nearest_sentences

    def extractive_summarization(self, transcript):
        """
        Performs extractive summarization on the given transcript.

        Args:
            transcript: A string containing the transcript.

        Returns:
            A list of strings containing the summary sentences.
        """

        parser = PlaintextParser.from_string(transcript, Tokenizer("english"))
        summarizer = LexRankSummarizer()
        summary_sentences = summarizer(
            parser.document, 15
        )  # 10% of the total sentences
        summary = [str(sentence) for sentence in summary_sentences]
        return summary

    def abstractive_summarization(self, transcript):
        """
        Performs Abstractive summarization on the given transcript.

        Args:
            transcript: A string containing the transcript.

        Returns:
            A list of strings containing the summary sentences.
        """
        summaries = self.abstract_summarizer.summarize(transcript)
        if len(summaries) >= 2:
            summarized_transcript = ".".join(summaries)
            summaries = self.abstract_summarizer.summarize(summarized_transcript)
        return self.extractive_summarization(".".join(summaries))


class TranscriptManagement:
    """
    A class for managing transcripts in Redis.

    Args:
        conn_str: The Redis connection string.
    """

    def __init__(self, conn_str):
        """
        Initializes a new TranscriptManagement object.

        Args:
            conn_str: The Redis connection string.
        """

        self.redis = redis.from_url(conn_str)

    def get_transcript(self, video_id):
        """
        Gets the transcript for the given video ID.

        Args:
            video_id: The ID of the video.

        Returns:
            A string containing the transcript of the video, or None if the transcript does not exist.
        """

        transcript_data = self.redis.get(video_id)
        if transcript_data:
            return json.loads(transcript_data.decode("utf-8"))["transcript"]
        return None

    def set_transcript(self, video_id, transcript):
        """
        Sets the transcript for the given video ID.

        Args:
            video_id: The ID of the video.
            transcript: The transcript of the video.
        """

        self.redis.set(video_id, json.dumps({"transcript": transcript}))

    def delete_transcript(self, video_id):
        """
        Deletes the transcript for the given video ID.

        Args:
            video_id: The ID of the video.
        """

        self.redis.delete(video_id)


class YouTubeVideo:
    """
    Represents a YouTube video.

    Attributes:
        url (str): The URL of the video.
        video_id (str): The ID of the video.
        audio_dir (str): The directory where the audio file for the video is stored.
        audio_path (str): The path to the audio file for the video.
        title (str): The title of the video.
        author (str): The author of the video.
        description (str): The description of the video.
        video_metadata (str): A string containing the metadata of the video.
        transcript (str): The transcript of the video.
        transcript_summary (str): A summary of the transcript of the video.
    """

    def __init__(self, url, audio_dir="/content/audio"):
        """
        Initializes a YouTubeVideo object.

        Args:
            url (str): The URL of the YouTube video.
            audio_dir (str, optional): The directory where the audio file will be stored. Default is "/content/audio".
        """
        self.url = url
        self.video_id = None
        self.audio_dir = audio_dir
        self.audio_path = None
        self.title = None
        self.author = None
        self.description = None
        self.video_metadata = None
        self.transcript = None
        self.transcript_summary = None
        self.sentences = None
        self.embeddings = None
        self.bm25 = None

    def _update_video_metadata(self, yt):
        """
        Updates video metadata attributes based on a YouTube video object.

        Args:
            yt (pytube.YouTube): A YouTube video object.
        """
        self.title = yt.title
        self.author = yt.author
        if yt.description:
            description = yt.description.split('.')
            if len(description) > 3:
                description = description[:3]
            self.description = description
        else:
            self.description = ""
        self.video_metadata = f""" Title: {self.title},
        Description: {' '.join(self.description)}"""

    def download_audio(self):
        """
        Downloads the audio of the YouTube video and stores it in the specified directory.
        """
        yt = YouTube(self.url)
        stream = yt.streams.filter(only_audio=True).first()
        stream.download(output_path=self.audio_dir, filename=f"{self.video_id}.mp3")
        self.audio_path = os.path.join(self.audio_dir, f"{self.video_id}.mp3")

    def _process_url(self):
        """
        Processes the URL to create the audio directory and update video metadata.
        """
        os.makedirs(self.audio_dir, exist_ok=True)
        yt = YouTube(self.url)
        self._update_video_metadata(yt)

class YouTubeVideoProcessor:
    """
    A class for processing YouTube videos.

    Attributes:
        asr_system: An ASR system for transcribing audio.
        semantic_system: A semantic system for understanding text.
        assistant: An assistant for answering questions.
        transcript_manager: The Transcript Manager Redis Cache Hosted on render.com (or wherever)
    """

    def __init__(self, asr_system, semantic_system, assistant, transcript_manager=transcript_manager):
        """
        Initializes a YouTubeVideoProcessor object.

        Args:
            asr_system: An Automatic Speech Recognition (ASR) system.
            semantic_system: A semantic system for understanding text.
            assistant: An assistant for answering questions.
        """
        self.asr_system = asr_system
        self.semantic_system = semantic_system
        self.assistant = assistant
        self.transcript_manager = transcript_manager

    def _populate_transcript(self, youtube_video):
        """
        Populates the transcript attribute of a YouTubeVideo object.

        Args:
            youtube_video (YouTubeVideo): The YouTube video to process.
        """
        youtube_video.video_id = youtube_video.url[youtube_video.url.rindex('?v=')+3:]
        transcript = self.transcript_manager.get_transcript(youtube_video.video_id)
        if transcript is None or isinstance(transcript, list):
            try:
                transcript_dict = YouTubeTranscriptApi.get_transcript(youtube_video.video_id)
                sentences = [x['text'] for x in transcript_dict]
                transcript = '.'.join(sentences)
            except:
                youtube_video.download_audio()
                transcript = self.asr_system.transcribe_audio(youtube_video.audio_path)
            self.transcript_manager.set_transcript(youtube_video.video_id, transcript)
        youtube_video.transcript = transcript

    def _summarize_transcript(self, youtube_video):
        """
        Summarizes the transcript and populates relevant attributes of a YouTubeVideo object.

        Args:
            youtube_video (YouTubeVideo): The YouTube video to process.
        """
        youtube_video.sentences = youtube_video.transcript.split('.')
        if len(youtube_video.sentences) == 1:
            youtube_video.sentences = self.semantic_system.abstract_summarizer._split_input_into_chunks(youtube_video.transcript, 1024)
        youtube_video.embeddings = self.semantic_system.model.encode(youtube_video.sentences)
        youtube_video.bm25 = self.semantic_system.lexical_system(youtube_video.sentences)
        youtube_video.transcript_summary = self.semantic_system.abstractive_summarization(youtube_video.transcript)

    def process_youtube_video(self, youtube_video):
        """
        Processes a YouTube video by populating its metadata, transcript, and summary.

        Args:
            youtube_video (YouTubeVideo): The YouTube video to process.
        """
        youtube_video._process_url()
        self._populate_transcript(youtube_video)
        self._summarize_transcript(youtube_video)


    def answer(self, youtube_video, query):
        """
        Generates an answer to a user's query based on the YouTube video and query.

        Args:
            youtube_video (YouTubeVideo): The YouTube video being processed.
            query (str): The user's query.

        Returns:
            str: The generated response.
        """
        context = ' '.join(self.semantic_system.get_nearest_sentences(youtube_video.sentences, query,
                                                                      precomputed_embeddings=youtube_video.embeddings, precomputed_lexical=youtube_video.bm25))
        prompt = f"""You are Summachatter, the YouTube Video Ask-me-Anything Bot.

            User: {query}

            **Video Information:**
            - Author: {youtube_video.author}
            - Title: {youtube_video.title}
            - Description: {' '.join(youtube_video.description)}

            **Extractive Summary:**
            {youtube_video.transcript_summary}

            **Context:**
            {context}

            Your task is to provide a concise, helpful, and polite response based on the relevant sentences from the YouTube video transcript.

            Remember:
            - Be informative and to the point.
            - Avoid starting with "Hi" or "Hello."

            Response: """
        response = self.assistant(prompt)
        return response

if __name__ == "__main__":
    assistant = Assistant(llm_function)
    console = Console()
    loop = asyncio.get_event_loop()
    display_content = lambda x: console.print(Markdown(x))
    chatgpt = lambda x: display_content(loop.run_until_complete(assistant.answer(x)))
    cross_encoder = CrossEncoder("cross-encoder/ms-marco-MiniLM-L-6-v2")
    sentence_transformer = SentenceTransformer("sentence-transformers/all-MiniLM-L12-v2")
    abstractive_summarizer = AbstractSummarizer()
    transcription_cache = TranscriptManagement(REDIS_CONN_STR)
    asr_system = ASRSystem()
    semantic_system = SentenceEmbeddingSystem(
        cross_encoder, sentence_transformer, abstractive_summarizer
    )
    youtube_processor = YouTubeVideoProcessor(
        asr_system, semantic_system, chatgpt, transcription_cache
    )
    url = "https://www.youtube.com/watch?v=Qc7AuFwtfqE"
    video = YouTubeVideo(url)
    youtube_processor.process_youtube_video(video)
    answer = youtube_processor.answer(video, "What is this video about?")
    print(answer)
    """
    This video covers a variety of AI news topics. It mentions that Coca-Cola has used artificial intelligence to
    create a new flavor of coke. Apple has introduced a feature called object capture that allows a camera to render 3D
    objects. Tesla's self-driving cars are based on pure AI learning from video data. Facebook plans to train its
    chatbot with CEO Mark Zuckerberg. Microsoft has taken responsibility for GitHub.copilot, a platform for code
    development. Amazon now requires authors to disclose the use of AI when publishing books on Kindle. Lastly, Russian
    search engine Yandex is releasing an AI-based bot named Yandex.GPT.
    """
